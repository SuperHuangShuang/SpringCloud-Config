package com.pinganfu.barleyonlineservice.biz.spellgroup;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.DefaultTuple;
import org.springframework.data.redis.connection.RedisZSetCommands.Tuple;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dianping.cat.Cat;
import com.pinganfu.barleyonlineservice.biz.job.SpellGroupJob;
import com.pinganfu.barleyonlineservice.biz.spellgroup.validate.SpellGroupFacadeValidate;
import com.pinganfu.barleyonlineservice.dal.domain.SpellGroupDO;
import com.pinganfu.barleyonlineservice.dal.domain.SpellGroupLaunchDO;
import com.pinganfu.barleyonlineservice.dal.domain.SpellGroupRewardDO;
import com.pinganfu.barleyonlineservice.dal.domain.SpellRewardRecordDO;
import com.pinganfu.barleyonlineservice.dal.model.RewardMessageModel;
import com.pinganfu.barleyonlineservice.dal.redis.BaseCache;
import com.pinganfu.barleyonlineservice.dal.redis.LimitCache;
import com.pinganfu.barleyonlineservice.dal.redis.key.SpellGroupCacheKey;
import com.pinganfu.barleyonlineservice.facade.common.page.Page;
import com.pinganfu.barleyonlineservice.facade.spellgroup.SpellGroupFacade;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.CustomerSpellGroupModel;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.SpellRewardRecordModel;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.WeChatMessageModel;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.JoinSpellGroupRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.LaunchSpellGroupRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.QueryCustomerSpellGroupRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.QuerySpellJoinNewCustomerRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.QuerySpellRewardRecordRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.JoinSpellGroupResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.LaunchSpellGroupResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.QueryCustomerSpellGroupResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.QuerySpellJoinNewCustomerResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.QuerySpellRewardRecordResponse;
import com.pinganfu.barleyonlineservice.integration.risk.AntiCheatFacadeClient;
import com.pinganfu.barleyonlineservice.integration.risk.convert.ConvertAntiCheatFacadeClient;
import com.pinganfu.barleyonlineservice.integration.risk.model.RishPushRewardGrantMessage;
import com.pinganfu.barleyonlineservice.integration.risk.model.RiskRewardGrantRequest;
import com.pinganfu.barleyonlineservice.service.spellgroup.SpellGroupCacheService;
import com.pinganfu.barleyonlineservice.service.spellgroup.SpellGroupLaunchService;
import com.pinganfu.barleyonlineservice.service.spellgroup.SpellGroupRewardService;
import com.pinganfu.barleyonlineservice.service.spellgroup.SpellGroupService;
import com.pinganfu.barleyonlineservice.service.spellgroup.SpellRewardRecordService;
import com.pinganfu.barleyonlineservice.util.ByteConvertUtil;
import com.pinganfu.barleyonlineservice.util.GenerateBizNoUtil;
import com.pinganfu.barleyonlineservice.util.TimeUtil;
import com.pinganfu.barleyonlineservice.util.TimeUtil.TimeFormatEnum;
import com.pinganfu.barleyonlineservice.util.constants.CodeCommonEnum;
import com.pinganfu.barleyonlineservice.util.constants.IntegerToStringEnum;
import com.pinganfu.barleyonlineservice.util.constants.NumberCommonEnum;
import com.pinganfu.barleyonlineservice.util.constants.RiskCommonEnum;
import com.pinganfu.barleyonlineservice.util.constants.SpellGroupBenefitCustomerTypeEnum;
import com.pinganfu.barleyonlineservice.util.constants.SpellGroupCustomerSourceEnum;
import com.pinganfu.barleyonlineservice.util.constants.SpellGroupJoinCustomerTypeEnum;
import com.pinganfu.barleyonlineservice.util.constants.SpellGroupLaunchStatusEnum;
import com.pinganfu.barleyonlineservice.util.constants.SpellGroupRewardStatusEnum;
import com.pinganfu.barleyonlineservice.util.constants.StringCommonEnum;
import com.pinganfu.barleyonlineservice.util.constants.SymbolCommonEnum;
import com.pinganfu.barleyonlineservice.util.constants.SystemCommonEnum;
import com.pinganfu.barleyonlineservice.util.constants.TypeCommonEnum;

@Component
@Service
public class SpellGroupFacadeImpl implements SpellGroupFacade {

    private static Logger logger = LoggerFactory.getLogger(SpellGroupFacadeImpl.class);

    @Autowired
    private BaseCache baseCache;
    @Autowired
    private SpellGroupJob spellGroupJob;
    @Autowired
    private AntiCheatFacadeClient antiCheatFacadeClient;
    @Autowired
    private SpellGroupLaunchService spellGroupLaunchService;
    @Autowired
    private SpellGroupCacheService spellGroupCacheService;
    @Autowired
    private SpellGroupRewardService spellGroupRewardService;
    @Autowired
    private SpellRewardRecordService spellRewardRecordService;
    @Autowired
    private SpellGroupService spellGroupService;
    @Autowired
    private LimitCache limitCache;

    private static final String CATTYPE = "SpellGroupFacade";
    private static final String CATSTATUSSUCCESS = "0";
    private static final String CATSTATUSFAIL = "1";

    @Override
    public LaunchSpellGroupResponse launchSpellGroup(LaunchSpellGroupRequest launchSpellGroupRequest) {

        LaunchSpellGroupResponse response = new LaunchSpellGroupResponse();

        // 入参校验
        String check = SpellGroupFacadeValidate.checkLaunchSpellGroup(launchSpellGroupRequest);
        if (!StringUtils.isBlank(check)) {
            response.setResponseCode(CodeCommonEnum.ARGS_INVALID_EXCEPTION.getCode());
            response.setMemo(check);
            logger.warn("SpellGroupFacadeImpl launchSpellGroup checkLaunchSpellGroup is [{}], response is [{}]", check,
                    response);
            return response;
        }
        Map<String, Long> rollBackKeyMap = new HashMap<String, Long>();
        logger.info("SpellGroupFacadeImpl launchSpellGroup launchSpellGroupRequest is [{}]", launchSpellGroupRequest);

        try {

            String currentKey = SpellGroupCacheKey.getCurrentKey(launchSpellGroupRequest.getCustomerId(), null,
                    SpellGroupCacheKey.CurrentType.LAUNCH);
            if (!baseCache.setNx(currentKey, IntegerToStringEnum.CURRENT_INSERT.getValue(),
                    NumberCommonEnum.ONE.getValue())) {
                response.setResponseCode(CodeCommonEnum.BASE_CURRENT.getCode());
                response.setMemo(CodeCommonEnum.BASE_CURRENT.getMemo());
                logger.warn("SpellGroupFacadeImpl.launchSpellGroup response = [{}]", response);
                return response;
            }

            // 内存获取规则信息，根据活动id
            SpellGroupDO spellGroupDO = spellGroupJob
                    .querySpellGroupRuleByActivityId(launchSpellGroupRequest.getActivityId());
            if (Objects.isNull(spellGroupDO)) {
                response.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getCode());
                response.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getMemo());
                logger.warn("SpellGroupFacadeImpl launchSpellGroup spellGroupDO is null, response is [{}]", response);
                return response;
            }

            logger.info("SpellGroupFacadeImpl launchSpellGroup ruleID = {}", spellGroupDO.getId());
            // 验证发起限次
            CodeCommonEnum checkResult = spellGroupCacheService.checkLaunchLimit(spellGroupDO,
                    launchSpellGroupRequest.getCustomerId(), rollBackKeyMap);
            if (!Objects.isNull(checkResult)) {
                response.setResponseCode(checkResult.getCode());
                response.setMemo(checkResult.getMemo());
                logger.warn("SpellGroupFacadeImpl launchSpellGroup response is [{}] ", response);
                return response;
            }

            String customerId = launchSpellGroupRequest.getCustomerId();
            String mobile = launchSpellGroupRequest.getMobile();
            String activityId = launchSpellGroupRequest.getActivityId();
            Date date = new Date();

            // 组装发起对象，发起个数为拼团营销表拼团数量
            Long spellGroupCount = spellGroupDO.getSpellGroupCount();

            // 查询 spellGroupCount 个连续的sequence
            List<Long> rewardRecordSequences = spellRewardRecordService
                    .querySpellRewardRecordSequences(spellGroupCount);

            List<SpellRewardRecordDO> spellRewardRecordDOList = new ArrayList<>();
            SpellRewardRecordDO spellRewardRecordDO = null;

            for (int i = 0; i < rewardRecordSequences.size(); i++) {
                spellRewardRecordDO = new SpellRewardRecordDO(spellGroupDO.getId(),
                        SpellGroupRewardStatusEnum.SPELL_GROUP_REWARD_STAY.getCode(), customerId, date, date);
                spellRewardRecordDO.setId(rewardRecordSequences.get(i));
                spellRewardRecordDOList.add(spellRewardRecordDO);
            }

            spellRewardRecordService.addBatchSpellRewardRecord(spellRewardRecordDOList);

            List<SpellGroupLaunchDO> spellGroupLaunchDOList = spellRewardRecordDOList.stream().map(record -> {
                SpellGroupLaunchDO spellGroupLaunchDO = new SpellGroupLaunchDO(spellGroupDO.getId(), customerId, mobile,
                        date, date, SpellGroupLaunchStatusEnum.SPELL_GROUP_LAUNCH.getValue(), activityId);
                spellGroupLaunchDO.setSpellGroupRwdRecId(record.getId());
                return spellGroupLaunchDO;
            }).collect(Collectors.toList());

            // 查询拼团发起表序列ID
            List<Long> spellGroupLaunchSequences = spellGroupLaunchService
                    .querySpellGroupLaunchSequences(spellGroupCount);

            for (int i = 0; i < spellGroupLaunchDOList.size(); i++) {
                SpellGroupLaunchDO spellGroupLaunchDO = spellGroupLaunchDOList.get(i);
                spellGroupLaunchDO.setId(spellGroupLaunchSequences.get(i));
                spellRewardRecordService.addSpellRewardRecordCache(customerId, activityId,
                        SpellGroupJoinCustomerTypeEnum.OLD_CUSTOMER.getCode(), spellGroupDO, null,
                        spellGroupLaunchSequences.get(i), mobile, SpellGroupBenefitCustomerTypeEnum.LAUNCH.getCode(),
                        new Date(), spellGroupLaunchDO.getSpellGroupRwdRecId(),
                        SpellGroupRewardStatusEnum.SPELL_GROUP_REWARD_STAY.getCode());
            }

            spellGroupLaunchService.addBatchSpellGroupLaunch(spellGroupLaunchDOList);

            // 初始化缓存
            initLaunchCache(spellGroupDO, customerId, activityId, spellGroupLaunchSequences);
            response.setSpellGroupLaunchIdList(spellGroupLaunchSequences);

            response.setResponseCode(CodeCommonEnum.SYS_SUCCESS.getCode());
            response.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
            logger.warn("SpellGroupFacadeImpl launchSpellGroup response is [{}]", response);

            Cat.logEvent(CATTYPE, launchSpellGroupRequest.getActivityId(), CATSTATUSSUCCESS,
                    "launchSpellGroup success");

            return response;
        } catch (Exception e) {

            Cat.logEvent(CATTYPE, launchSpellGroupRequest.getActivityId(), CATSTATUSFAIL,
                    "launchSpellGroup throw exception");

            response.setResponseCode(CodeCommonEnum.SYS_FAIL.getCode());
            response.setMemo(CodeCommonEnum.SYS_FAIL.getMemo());
            limitCache.rollBackKey(rollBackKeyMap);
            logger.error("SpellGroupFacadeImpl launchSpellGroup is error [{}]", response, e);
            return response;
        }
    }

    @Override
    public JoinSpellGroupResponse joinSpellGroup(JoinSpellGroupRequest joinSpellGroupRequest) {
        logger.info("SpellGroupFacadeImpl.joinSpellGroup; JoinSpellGroupRequest is [{}]", joinSpellGroupRequest);

        JoinSpellGroupResponse resp = new JoinSpellGroupResponse();
        String check = SpellGroupFacadeValidate.checkJoinSpellGroup(joinSpellGroupRequest);
        if (StringUtils.isNotBlank(check)) {
            logger.warn("SpellGroupFacadeImpl.joinSpellGroup checkJoinSpellGroup is {}", check);
            resp.setResponseCode(CodeCommonEnum.ARGS_INVALID_EXCEPTION.getCode());
            resp.setMemo(check);
            return resp;
        }
        Map<String, Long> rollBackKeyMap = new HashMap<String, Long>();
        try {

            String currentKey = SpellGroupCacheKey.getCurrentKey(joinSpellGroupRequest.getCustomerId(),
                    joinSpellGroupRequest.getRecommenderCustomerId(), SpellGroupCacheKey.CurrentType.JOIN);
            if (!baseCache.setNx(currentKey, IntegerToStringEnum.CURRENT_INSERT.getValue(),
                    NumberCommonEnum.ONE.getValue())) {
                resp.setResponseCode(CodeCommonEnum.BASE_CURRENT.getCode());
                resp.setMemo(CodeCommonEnum.BASE_CURRENT.getMemo());
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup response = [{}]", resp);
                return resp;
            }

            SpellGroupDO spellGroupDO = spellGroupJob
                    .querySpellGroupRuleByActivityId(joinSpellGroupRequest.getActivityId());
            if (Objects.isNull(spellGroupDO)) {
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup spellGroupDO is null");
                resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getCode());
                resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getMemo());
                return resp;
            }

            String recommenderCustomerId = joinSpellGroupRequest.getRecommenderCustomerId();

            setWeChatMessage(joinSpellGroupRequest, spellGroupDO);

            if (StringUtils.equals(joinSpellGroupRequest.getRecommenderCustomerId(),
                    joinSpellGroupRequest.getCustomerId())) {
                resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_SELF_JOIN.getCode());
                resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_SELF_JOIN.getMemo());
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup response = [{}]", resp);
                return resp;
            }
            if (StringUtils.equals(joinSpellGroupRequest.getRecommenderMobile(), joinSpellGroupRequest.getMobile())) {
                resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_SELF_JOIN.getCode());
                resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_SELF_JOIN.getMemo());
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup response = [{}]", resp);
                return resp;
            }

            // 验限
            CodeCommonEnum checkResult = spellGroupCacheService.checkLimitCountCache(spellGroupDO,
                    recommenderCustomerId, joinSpellGroupRequest.getCustomerId(), rollBackKeyMap);
            if (!Objects.isNull(checkResult)) {
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup checkResult = [{}]", checkResult);
                resp.setResponseCode(checkResult.getCode());
                resp.setMemo(checkResult.getMemo());
                return resp;
            }
            logger.info("SpellGroupFacadeImpl.joinSpellGroup checkLimitResult = [{}]", checkResult);

            String spellGroupJoinCustomerType = joinSpellGroupRequest.getType();
            String joinCustomerCode = SpellGroupBenefitCustomerTypeEnum.JOIN.getCode();

            String spellGroupLaunchMember = spellGroupLaunchService.querySpellGroupLaunchId(spellGroupDO,
                    recommenderCustomerId, joinSpellGroupRequest.getSpellGroupLaunchId());
            if (Objects.isNull(spellGroupLaunchMember)) {

                logger.warn("SpellGroupFacadeImpl.joinSpellGroup spellGroupLaunchId is null");
                if (!SpellGroupJoinCustomerTypeEnum.NEW_CUSTOMER.getCode().equals(spellGroupJoinCustomerType)) {
                    logger.warn("SpellGroupFacadeImpl.joinSpellGroup joinSpellGroupRequest.getType() is not 0 , [{}]",
                            joinSpellGroupRequest.getType());
                    resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_NOT_SPELL_GROUP.getCode());
                    resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_NOT_SPELL_GROUP.getMemo());
                    limitCache.rollBackKey(rollBackKeyMap);
                    return resp;
                }

                Long newCustomerJoinLimit = spellGroupDO.getNewCustomerJoinLimit();
                if (null == newCustomerJoinLimit
                        || newCustomerJoinLimit.compareTo(NumberCommonEnum.ONE.getValue()) != 0) {
                    logger.warn("SpellGroupFacadeImpl.joinSpellGroup newCustomerJoinLimit is not 1 , [{}]",
                            newCustomerJoinLimit);
                    resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_NOT_SPELL_GROUP.getCode());
                    resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_NOT_SPELL_GROUP.getMemo());
                    limitCache.rollBackKey(rollBackKeyMap);
                    return resp;
                }

                String spellGroupBenefitCustomerType = SpellGroupBenefitCustomerTypeEnum.JOIN.getCode();

                if (checkSpellGroupRisk(joinSpellGroupRequest)) {
                    logger.warn("SpellGroupFacadeImpl.joinSpellGroup risk return reject...");
                    spellGroupJoinCustomerType = SpellGroupJoinCustomerTypeEnum.RISK_CUSTOMER.getCode();
                    spellGroupBenefitCustomerType = SpellGroupJoinCustomerTypeEnum.RISK_CUSTOMER.getCode();
                }

                // 新用户逻辑
                String position = newCustomerLogic(null, joinSpellGroupRequest, spellGroupDO, rollBackKeyMap,
                        spellGroupJoinCustomerType, spellGroupBenefitCustomerType);

                bindNewCustomer(joinSpellGroupRequest, spellGroupDO);

                resp.setPosition(position);
                resp.setJoinCustomerType(spellGroupJoinCustomerType);
                resp.setResponseCode(CodeCommonEnum.SYS_SUCCESS.getCode());
                resp.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
                logger.info("SpellGroupFacadeImpl.joinSpellGroup resp is [{}]", resp);
                Cat.logEvent(CATTYPE, joinSpellGroupRequest.getActivityId(), CATSTATUSSUCCESS,
                        "joinSpellGroup success");
                return resp;
            }

            String[] members = spellGroupLaunchMember.split(SymbolCommonEnum.SYMBOL_AND.getCode());
            Date launchDate = TimeUtil.string2Date(members[1], TimeFormatEnum.SHORT_DATE_PATTERN_LINE.getFormat());
            Long spellGroupLaunchId = new Long(members[0]);

            // 参与人过风控
            if (checkSpellGroupRisk(joinSpellGroupRequest)) {
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup risk return reject...");
                spellGroupJoinCustomerType = SpellGroupJoinCustomerTypeEnum.RISK_CUSTOMER.getCode();
                joinCustomerCode = SpellGroupJoinCustomerTypeEnum.RISK_CUSTOMER.getCode();
            }

            logger.info("SpellGroupFacadeImpl.joinSpellGroup spellGroupJoinCustomerType is [{}]",
                    spellGroupJoinCustomerType);
            List<SpellGroupRewardDO> spellGroupRewardDOList = spellGroupRewardService.samplingReward(spellGroupDO,
                    spellGroupJoinCustomerType, joinCustomerCode);
            if (Objects.isNull(spellGroupRewardDOList) || spellGroupRewardDOList.isEmpty()) {
                logger.warn("SpellGroupFacadeImpl.joinSpellGroup spellGroupRewardDOList is null");
                resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_EVENT_BYE_NULL.getCode());
                resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_SPELLGROUP_EVENT_BYE_NULL.getMemo());
                return resp;
            }
            logger.info("SpellGroupFacadeImpl.joinSpellGroup spellGroupRewardDOList is [{}]", spellGroupRewardDOList);
            spellRewardRecordService.addSpellRewardRecordAndCache(joinSpellGroupRequest.getCustomerId(),
                    joinSpellGroupRequest.getActivityId(), spellGroupJoinCustomerType, spellGroupDO,
                    spellGroupRewardDOList.get(0).getBranchGroup(), spellGroupLaunchId,
                    joinSpellGroupRequest.getMobile(), null,
                    SpellGroupRewardStatusEnum.SPELL_GROUP_REWARD_STAY.getCode(), joinCustomerCode, launchDate, null,
                    null, joinSpellGroupRequest.getType());

            try {

                joinSpellGroupRequest.setType(spellGroupJoinCustomerType);
                spellGroupService.handleSpellGroupReward(spellGroupDO, joinSpellGroupRequest, spellGroupLaunchId,
                        launchDate);
            } catch (Exception e) {
                logger.error("SpellGroupFacadeImpl.joinSpellGroup flagSpellGroupRewardSend is error", e);
            }

            bindNewCustomer(joinSpellGroupRequest, spellGroupDO);

            resp.setJoinCustomerType(spellGroupJoinCustomerType);
            resp.setPosition(spellGroupRewardDOList.get(0).getBranchGroup());
            resp.setResponseCode(CodeCommonEnum.SYS_SUCCESS.getCode());
            resp.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
            logger.info("SpellGroupFacadeImpl.joinSpellGroup resp is [{}]", resp);

            Cat.logEvent(CATTYPE, joinSpellGroupRequest.getActivityId(), CATSTATUSSUCCESS, "joinSpellGroup success");
            return resp;
        } catch (Exception e) {
            Cat.logEvent(CATTYPE, joinSpellGroupRequest.getActivityId(), CATSTATUSFAIL, "joinSpellGroup success");

            logger.error("SpellGroupFacadeImpl.joinSpellGroup is error [{}]", e);
            limitCache.rollBackKey(rollBackKeyMap);
            resp.setResponseCode(CodeCommonEnum.SYS_FAIL.getCode());
            resp.setMemo(CodeCommonEnum.SYS_FAIL.getMemo());
            return resp;
        }
    }

    /**
     * 绑定新用户关系
     * 
     * @param joinSpellGroupRequest
     * @param spellGroupDO
     */
    private void bindNewCustomer(JoinSpellGroupRequest joinSpellGroupRequest, SpellGroupDO spellGroupDO) {
        try {
            if (StringUtils.equals(SpellGroupJoinCustomerTypeEnum.NEW_CUSTOMER.getCode(), joinSpellGroupRequest.getType())) {
                Long cacheEx = TimeUtil.getDateDiff(new Date(), TimeUtil.add(Calendar.DATE, spellGroupDO.getEndTime(),
                        NumberCommonEnum.THIRTY.getValue().intValue()), TimeFormatEnum.FORMAT_S.getFormat());
                String newCustomerKey = SpellGroupCacheKey.getNewCustomerKey(spellGroupDO.getActivityId(),
                        joinSpellGroupRequest.getRecommenderCustomerId());
                String newCustomerValue = SpellGroupCacheKey.getNewCustomerValue(joinSpellGroupRequest.getCustomerId());
                baseCache.zAdd(newCustomerKey, System.currentTimeMillis(), newCustomerValue);
                baseCache.expire(newCustomerKey, cacheEx);
            }
        } catch (Exception e) {
            logger.error("SpellGroupFacadeImpl.bindNewCustomer(); e = [{}]", e);
        }
    }

    private void setWeChatMessage(JoinSpellGroupRequest req, SpellGroupDO spellGroupDO) {
        try {
            if (StringUtils.isNotBlank(req.getNickName()) && StringUtils.isNotBlank(req.getHeadImageUrl())) {
                Long cacheEx = TimeUtil.getDateDiff(new Date(), TimeUtil.add(Calendar.DATE, spellGroupDO.getEndTime(),
                        NumberCommonEnum.THIRTY.getValue().intValue()), TimeFormatEnum.FORMAT_S.getFormat());
                String weChatMessageKey = SpellGroupCacheKey.getWeChatMessageKey(req.getActivityId(),
                        req.getCustomerId());
                baseCache.hSet(weChatMessageKey, SpellGroupCacheKey.getWeChatNickNameField(), req.getNickName());
                baseCache.hSet(weChatMessageKey, SpellGroupCacheKey.getWeChatHeadImageUrlField(),
                        req.getHeadImageUrl());
                baseCache.expire(weChatMessageKey, cacheEx);
            }
        } catch (Exception e) {
            logger.error("SpellGroupFacadeImpl.setWeChatMessage(); e = [{}]", e);
        }
    }

    @Override
    public QueryCustomerSpellGroupResponse queryCustomerSpellGroup(
            QueryCustomerSpellGroupRequest queryCustomerSpellGroupRequest) {

        QueryCustomerSpellGroupResponse resp = new QueryCustomerSpellGroupResponse();

        // 参数验证 activityId customerId
        String check = SpellGroupFacadeValidate.checkQueryCustomerSpellGroup(queryCustomerSpellGroupRequest);
        if (StringUtils.isNotBlank(check)) {
            logger.warn("SpellGroupFacadeImpl queryCustomerSpellGroup checkQueryCustomerSpellGroup is {}", check);
            resp.setResponseCode(CodeCommonEnum.ARGS_INVALID_EXCEPTION.getCode());
            resp.setMemo(check);
            return resp;
        }

        logger.info("SpellGroupFacadeImpl queryCustomerSpellGroup request = {}", queryCustomerSpellGroupRequest);

        String activityId = queryCustomerSpellGroupRequest.getActivityId();
        String customerId = queryCustomerSpellGroupRequest.getCustomerId();

        // 取规则
        SpellGroupDO spellGroupDO = spellGroupJob.querySpellGroupRuleByActivityId(activityId);
        if (Objects.isNull(spellGroupDO)) {
            logger.warn("SpellGroupFacadeImpl queryCustomerSpellGroup spellGroupDO is null");
            resp.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getCode());
            resp.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getMemo());
            return resp;
        }

        try {

            String dismantleRecordKey = SpellGroupCacheKey.getDismantleRecordKey(activityId, customerId);
            Map<String, String> recordRewardMap = baseCache.hGetAll(dismantleRecordKey);
            Iterator<Entry<String, String>> iterator = recordRewardMap.entrySet().iterator();

            List<CustomerSpellGroupModel> spellGroupModelList = new ArrayList<CustomerSpellGroupModel>();
            int launchSpellGroupLimit = 0;
            while (iterator.hasNext()) {
                Entry<String, String> next = iterator.next();
                CustomerSpellGroupModel model = convertModel(next, customerId, spellGroupDO.getAging(),
                        dismantleRecordKey);
                if (Objects.isNull(model)) {
                    continue;
                }
                if (StringUtils.isBlank(model.getPosition())) {
                    launchSpellGroupLimit++;
                }
                // 红包页面翻转状态
                if (SpellGroupCustomerSourceEnum.ENVELOPE_PAGE.getCode()
                        .equals(queryCustomerSpellGroupRequest.getSource())
                        && SpellGroupRewardStatusEnum.OPEN_STATUS_Y.getCode().equals(model.getOpenStatus())) {
                    try {

                        String json = baseCache.hGet(dismantleRecordKey, next.getKey());
                        if (StringUtils.isBlank(json)) {
                            launchSpellGroupLimit--;
                            logger.warn("SpellGroupFacadeImpl queryCustomerSpellGroup json is null");
                            return null;
                        }
                        JSONObject jsonObject = JSONObject.parseObject(json);
                        String readStatus = jsonObject.getString("readStatus");
                        model.setReadStatus(StringUtils.isBlank(readStatus)
                                ? SpellGroupRewardStatusEnum.READ_FLAG_N.getCode() : readStatus);

                        jsonObject.put("readStatus", SpellGroupRewardStatusEnum.READ_FLAG_Y.getCode());
                        baseCache.hSet(dismantleRecordKey, next.getKey(), jsonObject.toString());
                    } catch (Exception e) {
                        logger.error(
                                "SpellGroupFacadeImpl queryCustomerSpellGroup baseCache.hSet is error, dismantleRecordKey is [{}], next.getKey() is [{}]",
                                dismantleRecordKey, next.getKey(), e);
                    }
                }
                spellGroupModelList.add(model);
            }

            if (!Objects.isNull(spellGroupModelList) && !spellGroupModelList.isEmpty()) {
                spellGroupModelList.sort((CustomerSpellGroupModel h1, CustomerSpellGroupModel h2) -> h2.getOpenStatus()
                        .compareTo(h1.getOpenStatus()));
            }

            resp.setLaunchSpellGroupLimit(launchSpellGroupLimit);
            resp.setCustomerSpellGroupModelList(spellGroupModelList);
            resp.setResponseCode(CodeCommonEnum.SYS_SUCCESS.getCode());
            resp.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
            logger.info("SpellGroupFacadeImpl queryCustomerSpellGroup resp = {}", resp);
            return resp;
        } catch (Exception e) {
            logger.error("SpellGroupFacadeImpl queryCustomerSpellGroup error", e);
            resp.setResponseCode(CodeCommonEnum.SYS_FAIL.getCode());
            resp.setMemo(CodeCommonEnum.SYS_FAIL.getMemo());
            return resp;
        }
    }

    private CustomerSpellGroupModel convertModel(Entry<String, String> entry, String customerId, Long aging,
            String dismantleRecordKey) throws ParseException {

        String field = entry.getKey();
        String[] message = field.split(SymbolCommonEnum.SYMBOL_AND.getCode());

        CustomerSpellGroupModel model = new CustomerSpellGroupModel();
        model.setPosition(StringCommonEnum.STRING_NULL.getValue().equals(message[0]) ? null : message[0]);
        model.setJoinCustomerType(message[1]);
        model.setBenefitCustomerType(message[2]);
        String openStatus = SpellGroupRewardStatusEnum.OPEN_STATUS_Y.getCode();
        if (StringUtils.isBlank(model.getPosition())) {
            openStatus = SpellGroupRewardStatusEnum.OPEN_STATUS_N.getCode();
        }

        Date launchTime = TimeUtil.string2Date(message[3], TimeFormatEnum.SHORT_DATE_PATTERN_LINE.getFormat());
        Long agingDay = aging - NumberCommonEnum.ONE.getValue();
        Date agingDate = TimeUtil.add(Calendar.DATE, TimeUtil.getEndTime(launchTime), agingDay.intValue());

        logger.warn("launchTime is [{}], agingDay is [{}], agingDate is [{}]", launchTime, agingDay, agingDate);
        logger.warn("dismantleRecordKey is [{}], field is [{}]", dismantleRecordKey, field);
        if (new Date().compareTo(agingDate) > 0) {
            logger.warn("SpellGroupFacadeImpl queryCustomerSpellGroup convertModel SpellGroupLaunchId is invalid [{}]",
                    model.getSpellGroupLaunchId());
            try {
                baseCache.hDel(dismantleRecordKey, field);
            } catch (Exception e) {
                logger.error(
                        "SpellGroupFacadeImpl queryCustomerSpellGroup baseCache.hDel is error, dismantleRecordKey is [{}], field is [{}]",
                        dismantleRecordKey, field, e);
            }
            return null;
        }

        model.setSpellGroupLaunchId(
                StringUtils.isBlank(message[4]) || StringCommonEnum.STRING_NULL.getValue().equals(message[4]) ? null
                        : Long.valueOf(message[4]));

        model.setOpenStatus(openStatus);
        String json = entry.getValue();
        if (StringUtils.isBlank(json)) {
            logger.warn("SpellGroupFacadeImpl queryCustomerSpellGroup json is null");
            return null;
        }
        JSONObject jsonObject = JSONObject.parseObject(json);
        model.setReadStatus(jsonObject.getString("readStatus"));
        model.setStatus(jsonObject.getString("status"));

        return model;
    }

    private ConvertAntiCheatFacadeClient pushMessageToRisk(JoinSpellGroupRequest joinSpellGroupRequest) {

        String activityInfo = antiCheatFacadeClient.buildActivityInfo(joinSpellGroupRequest.getActivityId(),
                RiskCommonEnum.RISK_REWARD_TYPE_B.getCode());

        ConvertAntiCheatFacadeClient convertAntiCheatFacadeClient = new ConvertAntiCheatFacadeClient();
        convertAntiCheatFacadeClient.setOriginalJson(JSON.toJSONString(joinSpellGroupRequest));
        convertAntiCheatFacadeClient.setCheckType(RiskCommonEnum.RISK_CHECK_TYPE_R.getCode());
        convertAntiCheatFacadeClient.setRecommenderId(joinSpellGroupRequest.getRecommenderCustomerId());
        convertAntiCheatFacadeClient.setCustomerId(joinSpellGroupRequest.getCustomerId());
        convertAntiCheatFacadeClient.setCustomerFlag(TypeCommonEnum.TYPE_IS_CUSTOMERFLAG_1.getCode());
        convertAntiCheatFacadeClient.setOperationId(RiskCommonEnum.RISK_OPERATION_TYPE_B.getCode());
        convertAntiCheatFacadeClient.setActivityInfo(activityInfo);
        convertAntiCheatFacadeClient.setActivityId(joinSpellGroupRequest.getActivityId());
        convertAntiCheatFacadeClient.setChannelId(joinSpellGroupRequest.getChannel());
        convertAntiCheatFacadeClient.setCheckIdentityNoFlag(RiskCommonEnum.RISK_IS_RISKCHECK_Y.getCode());
        convertAntiCheatFacadeClient.setRewardGrantFlag(RiskCommonEnum.RISK_REWARD_GRANT_FLAG_EX.getCode());

        RishPushRewardGrantMessage pushRequest = convertAntiCheatFacadeClient.convertRishPushRewardGrantMessage();
        antiCheatFacadeClient.pushMessageToRisk(pushRequest, JSON.toJSONString(joinSpellGroupRequest));
        return convertAntiCheatFacadeClient;
    }

    private boolean checkSpellGroupRisk(JoinSpellGroupRequest joinSpellGroupRequest) {
        ConvertAntiCheatFacadeClient convertAntiCheatFacadeClient = pushMessageToRisk(joinSpellGroupRequest);
        RiskRewardGrantRequest rewardGrantRequest = convertAntiCheatFacadeClient.convertRiskRewardGrantRequest();
        return antiCheatFacadeClient.isCheckRisk(rewardGrantRequest);
    }

    @Transactional
    private String newCustomerLogic(Long spellGroupLaunchId, JoinSpellGroupRequest joinSpellGroupRequest,
            SpellGroupDO spellGroupDO, Map<String, Long> rollBackKeyMap, String spellGroupJoinCustomerType,
            String spellGroupBenefitCustomerType) {

        // 发起人发奖
        String launchRequestId = GenerateBizNoUtil.getBizNo(SystemCommonEnum.BARLEYONLINE_SERVICE.getCode());
        // 标记新人比成团的关系 只要是唯一值就可以
        String marking = launchRequestId;
        List<SpellGroupRewardDO> spellGroupLaunchList = giveRewardNewCustomer(joinSpellGroupRequest,
                spellGroupJoinCustomerType, SpellGroupBenefitCustomerTypeEnum.LAUNCH.getCode(), spellGroupDO,
                spellGroupLaunchId, launchRequestId, marking, SpellGroupBenefitCustomerTypeEnum.LAUNCH.getCode(), null);

        // 参与人发奖
        String requestId = GenerateBizNoUtil.getBizNo(SystemCommonEnum.BARLEYONLINE_SERVICE.getCode());
        List<SpellGroupRewardDO> spellGroupRewardDOList = giveRewardNewCustomer(joinSpellGroupRequest,
                spellGroupJoinCustomerType, spellGroupBenefitCustomerType, spellGroupDO, spellGroupLaunchId, requestId,
                marking, SpellGroupBenefitCustomerTypeEnum.JOIN.getCode(), joinSpellGroupRequest.getType());
        logger.info("SpellGroupFacadeImpl.joinSpellGroup response is [{}]", spellGroupRewardDOList);
        if (Objects.isNull(spellGroupRewardDOList) || spellGroupRewardDOList.isEmpty()) {
            return null;
        }
        String initiator = joinSpellGroupRequest.getRecommenderCustomerId();
        String helper = joinSpellGroupRequest.getCustomerId();
        spellGroupService.giveSpellGroupReward(joinSpellGroupRequest.getRecommenderCustomerId(),
                joinSpellGroupRequest.getRecommenderMobile(), joinSpellGroupRequest.getActivityId(),
                spellGroupLaunchList, launchRequestId, null, initiator, helper, spellGroupDO.getEndTime(), null);
        spellGroupService.giveSpellGroupReward(joinSpellGroupRequest.getCustomerId(), joinSpellGroupRequest.getMobile(),
                joinSpellGroupRequest.getActivityId(), spellGroupRewardDOList, requestId, null, initiator, helper,
                spellGroupDO.getEndTime(), null);

        return spellGroupRewardDOList.get(0).getBranchGroup();
    }

    private List<SpellGroupRewardDO> giveRewardNewCustomer(JoinSpellGroupRequest joinSpellGroupRequest,
            String spellGroupJoinCustomerType, String benefitCustomerType, SpellGroupDO spellGroupDO,
            Long spellGroupLaunchId, String requestId, String marking, String markingType, String joinType) {

        // 参与人发奖
        logger.info("SpellGroupFacadeImpl.joinSpellGroup spellGroupJoinCustomerType is [{}]",
                spellGroupJoinCustomerType);

        List<SpellGroupRewardDO> spellGroupRewardDOList = spellGroupRewardService.samplingReward(spellGroupDO,
                spellGroupJoinCustomerType,
                spellGroupJoinCustomerType == SpellGroupJoinCustomerTypeEnum.RISK_CUSTOMER.getCode()
                        ? spellGroupJoinCustomerType : benefitCustomerType);
        if (Objects.isNull(spellGroupRewardDOList) || spellGroupRewardDOList.isEmpty()) {
            logger.warn("SpellGroupFacadeImpl.joinSpellGroup spellGroupRewardDOList is null");
            return null;
        }
        String customerId = joinSpellGroupRequest.getCustomerId();
        String mobile = joinSpellGroupRequest.getMobile();
        if (SpellGroupBenefitCustomerTypeEnum.LAUNCH.getCode().equals(benefitCustomerType)) {
            customerId = joinSpellGroupRequest.getRecommenderCustomerId();
            mobile = joinSpellGroupRequest.getRecommenderMobile();
        }
        logger.info("SpellGroupFacadeImpl.joinSpellGroup spellGroupRewardDOList is [{}]", spellGroupRewardDOList);

        spellRewardRecordService.addSpellRewardRecordAndCache(customerId, joinSpellGroupRequest.getActivityId(),
                spellGroupJoinCustomerType, spellGroupDO, spellGroupRewardDOList.get(0).getBranchGroup(),
                spellGroupLaunchId, mobile, requestId, SpellGroupRewardStatusEnum.SPELL_GROUP_REWARD_SUCCESS.getCode(),
                benefitCustomerType, new Date(), marking, markingType, joinType);

        return spellGroupRewardDOList;
    }

    private void initLaunchCache(SpellGroupDO spellgroupDO, String customerId, String activityId,
            List<Long> launchIdList) {

        // 获取红包有效期并计算红包有效期(包括当天,所以-1)
        Long agingDay = spellgroupDO.getAging() - NumberCommonEnum.ONE.getValue();
        Date agingDate = TimeUtil.add(Calendar.DATE, TimeUtil.getEndTime(new Date()), agingDay.intValue());
        Long cacheEx = TimeUtil.getDateDiff(new Date(), agingDate, TimeFormatEnum.FORMAT_S.getFormat());
        Long cacheZsetEx = TimeUtil.getDateDiff(new Date(),
                TimeUtil.add(Calendar.DATE, spellgroupDO.getEndTime(), NumberCommonEnum.THIRTY.getValue().intValue()),
                TimeFormatEnum.FORMAT_S.getFormat());
        logger.info("initLaunchCache cacheEx is [{}]", cacheEx);
        String spellGroupLaunchZsetKey = SpellGroupCacheKey.getSpellGroupLaunchZsetKey(customerId, activityId);
        logger.info("initLaunchCache spellGroupLaunchZsetKey is [{}]", spellGroupLaunchZsetKey);
        Map<String, String> nxMap = new HashMap<>();
        Set<Tuple> tupelSet = new HashSet<>();
        launchIdList.stream().forEach(e -> {

            String member = SpellGroupCacheKey.getLaunchMember(e,
                    TimeUtil.date2String(TimeFormatEnum.SHORT_DATE_PATTERN_LINE.getFormat()));
            logger.info("initLaunchCache member is[{}]", member);
            Tuple tuple = new DefaultTuple(ByteConvertUtil.stringToByte(member),
                    Double.valueOf(NumberCommonEnum.ZERO.getValue()));
            tupelSet.add(tuple);

            String spellGroupLimitKey = SpellGroupCacheKey.getSpellGroupLimitKey(activityId, e, customerId);
            logger.info("initLaunchCache spellGroupLimitKey is[{}]", spellGroupLimitKey);
            nxMap.put(spellGroupLimitKey, String.valueOf(NumberCommonEnum.ZERO.getValue()));
        });

        logger.info("initLaunchCache zAddBatch tupelSet is [{}]", tupelSet);
        baseCache.zAddBatch(spellGroupLaunchZsetKey, tupelSet);
        baseCache.expire(spellGroupLaunchZsetKey, cacheZsetEx);

        logger.info("initLaunchCache nxMap is [{}]", nxMap);
        baseCache.setNxBatch(nxMap, cacheEx);
    }

    @Override
    public QuerySpellJoinNewCustomerResponse querySpellJoinNewCustomer(QuerySpellJoinNewCustomerRequest request) {
        QuerySpellJoinNewCustomerResponse response = new QuerySpellJoinNewCustomerResponse();

        try {
            String check = SpellGroupFacadeValidate.checkQuerySpellJoinNewCustomer(request);

            if (!StringUtils.isBlank(check)) {
                logger.info("SpellGroupFacadeImpl.querySpellJoinNewCustomer; check is [{}]", check);
                response.setResponseCode(CodeCommonEnum.ARGS_INVALID_EXCEPTION.getCode());
                response.setMemo(check);
                return response;
            }
            logger.info("SpellGroupFacadeImpl.querySpellJoinNewCustomer; request is [{}]", request);

            SpellGroupDO spellGroupDO = spellGroupJob.querySpellGroupRuleByActivityId(request.getActivityId());
            if (Objects.isNull(spellGroupDO)) {
                response.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getCode());
                response.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getMemo());
                logger.warn("SpellGroupFacadeImpl.querySpellJoinNewCustomer; spellGroupDO is null, response is [{}]", response);
                return response;
            }

            response.setWeChatMessageModelList(new ArrayList<WeChatMessageModel>());
            response.setPage(new Page());

            initPage(request.getPage(), SpellGroupCacheKey.getNewCustomerKey(request.getActivityId(), request.getCustomerId()), response.getPage());

            Map<String, Double> customerMessageMap = zRevRangeWithSores(SpellGroupCacheKey.getNewCustomerKey(request.getActivityId(), request.getCustomerId()), response.getPage());
            if (null == customerMessageMap || customerMessageMap.isEmpty()) {
                logger.info("SpellGroupFacadeImpl.querySpellJoinNewCustomer; customerMessageMap is null");
                response.setResponseCode((CodeCommonEnum.SYS_SUCCESS.getCode()));
                response.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
                return response;
            }

            addWeChatMessageModel(request.getActivityId(), customerMessageMap.keySet(), response.getWeChatMessageModelList());
        } catch (Exception e) {
            logger.error("SpellGroupFacadeImpl.querySpellJoinNewCustomer; e is [{}]", e);
            response.setResponseCode(CodeCommonEnum.SYS_FAIL.getCode());
            response.setMemo(CodeCommonEnum.SYS_FAIL.getMemo());
            return response;
        }

        response.setResponseCode(CodeCommonEnum.SYS_SUCCESS.getCode());
        response.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
        logger.info("SpellGroupFacadeImpl.querySpellJoinNewCustomer; response is [{}]", response);
        return response;
    }

    private Map<String, Double> zRevRangeWithSores(String key, Page page) {
        if (page.getTotalCount() <= 0) {
            return new HashMap<String, Double>();
        }
        return baseCache.zRevRangeWithScores(key, (page.getPageNo() - 1) * page.getPageSize(), page.getPageNo() * page.getPageSize() - 1);
    }

    private void initPage(Page requestPage, String rewardKey, Page page) {
        Integer pageSize = requestPage.getPageSize();
        Integer pageNo = requestPage.getPageNo();
        if (!(invalid(pageSize))) {
            page.setPageSize(pageSize);
        }
        if (!(invalid(pageNo))) {
            page.setPageNo(pageNo);
        }
        Long totalCount = baseCache.zCard(rewardKey);
        page.setTotalCount(totalCount);
        page.setTotalPage((totalCount - 1) / page.getPageSize() + 1);
    }

    private boolean invalid(Integer integer) {
        return Objects.isNull(integer) || integer <= 0;
    }

    @Override
    public QuerySpellRewardRecordResponse querySpellRewardRecord(QuerySpellRewardRecordRequest request) {
        QuerySpellRewardRecordResponse response = new QuerySpellRewardRecordResponse();

        try {
            String check = SpellGroupFacadeValidate.checkQuerySpellRewardRecord(request);

            if (!StringUtils.isBlank(check)) {
                logger.info("SpellGroupFacadeImpl.querySpellRewardRecord; check is [{}]", check);
                response.setResponseCode(CodeCommonEnum.ARGS_INVALID_EXCEPTION.getCode());
                response.setMemo(check);
                return response;
            }
            logger.info("SpellGroupFacadeImpl.querySpellRewardRecord; request is [{}]", request);

            SpellGroupDO spellGroupDO = spellGroupJob.querySpellGroupRuleByActivityId(request.getActivityId());
            if (Objects.isNull(spellGroupDO)) {
                response.setResponseCode(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getCode());
                response.setMemo(CodeCommonEnum.BUSINESS_EXCEPTION_RULE_NOT_EXSITED.getMemo());
                logger.warn("SpellGroupFacadeImpl.querySpellRewardRecord; spellGroupDO is null, response is [{}]", response);
                return response;
            }

            response.setSpellRewardRecordModelList(new ArrayList<SpellRewardRecordModel>());
            response.setPage(new Page());

            initPage(request.getPage(), SpellGroupCacheKey.getRewardKey(request.getActivityId(), request.getCustomerId()), response.getPage());

            Map<String, Double> rewardMessageMap = zRevRangeWithSores(SpellGroupCacheKey.getRewardKey(request.getActivityId(), request.getCustomerId()), response.getPage());
            if (null == rewardMessageMap || rewardMessageMap.isEmpty()) {
                logger.info("SpellGroupFacadeImpl.querySpellRewardRecord; rewardMessageMap is null");
                response.setResponseCode((CodeCommonEnum.SYS_SUCCESS.getCode()));
                response.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());
                return response;
            }

            for (Entry<String, Double> rewardMessageEntry : rewardMessageMap.entrySet()) {
                RewardMessageModel rewardMessageModel = SpellGroupCacheKey.getRewardValue(rewardMessageEntry.getKey());
                SpellRewardRecordModel spellRewardRecordModel = new SpellRewardRecordModel();
                response.getSpellRewardRecordModelList().add(spellRewardRecordModel);

                spellRewardRecordModel.setRequestId(rewardMessageModel.getRequestId());
                spellRewardRecordModel.setCreateTime(new Date(rewardMessageEntry.getValue().longValue()));
                spellRewardRecordModel.setWeChatMessageModelList(new ArrayList<WeChatMessageModel>());

                addWeChatMessageModel(request.getActivityId(), rewardMessageModel.getCustomerIdList(), spellRewardRecordModel.getWeChatMessageModelList());
            }
        } catch (Exception e) {
            logger.error("SpellGroupFacadeImpl.querySpellRewardRecord; e is [{}]", e);
            response.setResponseCode(CodeCommonEnum.SYS_FAIL.getCode());
            response.setMemo(CodeCommonEnum.SYS_FAIL.getMemo());
            return response;
        }

        response.setResponseCode(CodeCommonEnum.SYS_SUCCESS.getCode());
        response.setMemo(CodeCommonEnum.SYS_SUCCESS.getMemo());

        logger.info("SpellGroupFacadeImpl.querySpellRewardRecord; response is [{}]", response);
        return response;
    }

    private void addWeChatMessageModel(String activityId, Collection<String> customerIds, List<WeChatMessageModel> weChatMessageModelList) {
        for (String customerId : customerIds) {
            WeChatMessageModel weChatMessageModel = new WeChatMessageModel();
            weChatMessageModelList.add(weChatMessageModel);
            setWeChatMessageModel(activityId, customerId, weChatMessageModel);
        }
    }

    private void setWeChatMessageModel(String activityId, String customerId, WeChatMessageModel weChatMessageModel) {
        weChatMessageModel.setCustomerId(customerId);
        Map<String, String> weChatMessage = baseCache.hGetAll(SpellGroupCacheKey.getWeChatMessageKey(activityId, customerId));
        if (!Objects.isNull(weChatMessage) && !weChatMessage.isEmpty()) {
            weChatMessageModel.setNickName(weChatMessage.get(SpellGroupCacheKey.getWeChatNickNameField()));
            weChatMessageModel.setHeadImageUrl(weChatMessage.get(SpellGroupCacheKey.getWeChatHeadImageUrlField()));
        }
    }
}
