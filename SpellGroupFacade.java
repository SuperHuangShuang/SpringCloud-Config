package com.pinganfu.barleyonlineservice.facade.spellgroup;

import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.JoinSpellGroupRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.LaunchSpellGroupRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.QueryCustomerSpellGroupRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.QuerySpellJoinNewCustomerRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.request.QuerySpellRewardRecordRequest;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.JoinSpellGroupResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.LaunchSpellGroupResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.QueryCustomerSpellGroupResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.QuerySpellJoinNewCustomerResponse;
import com.pinganfu.barleyonlineservice.facade.spellgroup.model.response.QuerySpellRewardRecordResponse;

/**
 * 拼团营销
 * 
 * @author ZHANGYIJIE498
 *
 */
public interface SpellGroupFacade {

    /**
     * 发起拼团营销
     * 
     * @param launchSpellGroupRequest
     * @return
     */
    public LaunchSpellGroupResponse launchSpellGroup(LaunchSpellGroupRequest launchSpellGroupRequest);

    /**
     * 参与拼团营销
     * 
     * @param joinSpellGroupRequest
     * @return
     */
    public JoinSpellGroupResponse joinSpellGroup(JoinSpellGroupRequest joinSpellGroupRequest);

    /**
     * 查询用户拼团详情
     * 
     * @param queryCustomerSpellGroupRequest
     * @return
     */
    public QueryCustomerSpellGroupResponse queryCustomerSpellGroup(QueryCustomerSpellGroupRequest queryCustomerSpellGroupRequest);

    /**
     * 查询奖励记录
     * 
     * @param QuerySpellRewardRecordRequest
     * @return
     */
    public QuerySpellRewardRecordResponse querySpellRewardRecord(QuerySpellRewardRecordRequest QuerySpellRewardRecordRequest);

    /**
     * 查询帮(我)拆新用户
     * 
     * @param querySpellJoinNewCustomerRequest
     * @return
     */
    public QuerySpellJoinNewCustomerResponse querySpellJoinNewCustomer(QuerySpellJoinNewCustomerRequest querySpellJoinNewCustomerRequest);

}
